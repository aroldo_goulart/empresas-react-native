export type LoginResponse = {
    data:  {
        errors?: string[],
        enterprise?: any,
        investor?: {
            balance: number
            city: string,
            country: string,
            email: string,
            first_access: boolean,
            id: number,
            investor_name: string,
            photo: string | null,
            portfolio: {
                enterprises: []
                enterprises_number: number
            },
            portfolio_value: number
            super_angel: boolean
        },
        success: boolean
    },
    headers: {
        "access-token": string,
        client: string,
        uid: string,
    }
}


export type Enterprise = {
    city: string,
    country: string,
    description: string,
    email_enterprise: string | null,
    enterprise_name: string,
    enterprise_type: {
        id: number,
        enterprise_type_name: string
    },
    facebook: string | null,
    id: number,
    linkedin: string | null,
    own_enterprise: string | null,
    phone: string | null,
    photo: string,
    share_price: number,
    twitter: string | null
    value: number
}


export type StandardResponse = {
    data: {
        enterprises: Enterprise[],
        error: string[],
    }
} 