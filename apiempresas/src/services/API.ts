import Axios from "axios"
import { StandardResponse, LoginResponse } from "./types";

export const BASE_URL = 'https://empresas.ioasys.com.br'

const API = Axios.create({
    baseURL: `${BASE_URL}/api/v1/`,
    validateStatus: function (status) {
        return status >= 200 && status < 402
    }
});

export async function Login(email: string, password:string) {
    //console.log("calling api.. login")
    const { data, headers }: LoginResponse = await API.post("users/auth/sign_in", 
        {
            email,
            password
        }   
    )

    API.defaults.headers.get['uid'] = headers.uid
    API.defaults.headers.get['access-token'] = headers["access-token"]
    API.defaults.headers.get['client'] = headers.client
    
    return data
}

export async function GetCompanies() {
    //console.log("calling api.. GetCompanies")
    const { data }: StandardResponse  = await API.get("enterprises")

    return data
}

export async function GetCompany(id: number) {
    //console.log("calling api.. GetCompany")
    const { data }: StandardResponse = await API.get(`enterprises/${id}`)

    return data
}

export async function SearchCompany(name: string) {
    //console.log("calling api.. Search")
    const { data }: StandardResponse = await API.get(`enterprises/?name=${name}`)
    
    return data
}
