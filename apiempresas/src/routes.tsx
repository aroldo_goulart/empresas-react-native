
import React, { useState } from 'react'
import { DefaultTheme, NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import CompaniesScreen from "./pages/companies"
import LoginScreen from "./pages/login"

const Stack = createStackNavigator();

function Routes() {
  const [initialRouteName] = useState<string>('Login')

  const MyTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#0085ff',
      background: "#eee",
      card: '#fff',
      lightText: "#fff",
      mediumText: "#223333",
      darkText: "#191919",
      errorText: "#dd1119"
    },
  };
  
  return (
    <NavigationContainer theme={MyTheme}>
      <Stack.Navigator initialRouteName={initialRouteName}>
        <Stack.Screen 
          name="Login" 
          component={LoginScreen} 
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen 
          name="Companies" 
          component={CompaniesScreen} 
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>    
    </NavigationContainer>
  );
}

export default Routes