import React, { useEffect, useState } from 'react'
import { useTheme } from '@react-navigation/native'
import { View, Text, Image, Dimensions, Keyboard, RefreshControl, StyleSheet, Animated } from 'react-native'
import { FlatList, TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import PopUp from '../../component/popup'
import SearchList from '../../component/searchlist'
import { BASE_URL, GetCompanies, SearchCompany } from '../../services/API'
import { Enterprise } from '../../services/types'
import Warning from '../../component/warning'

interface CompaniesProps {

}

function Companies(props:CompaniesProps) {
    const { colors } = useTheme();
    const { width } = Dimensions.get("screen")
    const [enterprise, setEnterprise] = useState<Enterprise[]>()
    const [selectedEnterprise, setSelectedEnterprise] = useState<Enterprise>()
    const [textSearch, setTextSearch] = useState<string>("")
    const [textSearched, setTextSearched] = useState<string>("")
    const [loading, setLoading] = useState<boolean>(false)

    useEffect(() => {
        getDataCompanies()
    }, [])

    async function getDataCompanies() {
        setLoading(true)
        let { enterprises } = await GetCompanies()
        setEnterprise(enterprises)
        setLoading(false)
    }   
    
    const handlerClickCard = (item: Enterprise) => {
        setSelectedEnterprise(item)
    }
    
    const searchClick = async () => {
        const { enterprises } = await SearchCompany(textSearch)
        Keyboard.dismiss()
        setTextSearched(textSearch)
        setEnterprise(enterprises)
    }

    const handlerCloseSearch = () => {
        getDataCompanies()
        setTextSearched("")
        setTextSearch("")
    }

    const styles = StyleSheet.create({
        searchContainer: { 
            marginTop: 18, 
            marginHorizontal: 20, 
            borderRadius: 5, 
            backgroundColor: colors.card,
            flexDirection:"row",
            alignItems: 'center',
            justifyContent: "space-between",
        },
        searchTextInput: {
            paddingHorizontal: 10,
            color: colors.darkText,
            width: "80%"
        },
        iconNext: {
            height: 15,
            width: 15,
            alignItems: "center"
        },
        searchTouch: { padding: 14 },
        refreshControl: {
            zIndex: 30,
        },
        card: {
            backgroundColor: colors.card,
            paddingBottom: 10,
            margin: 20,
            marginTop: 10,
            marginBottom: 1,
            width: width*0.9,
            borderTopRightRadius: 5,
            borderTopLeftRadius: 5
        },
        image: { 
            height: 135,
            flex: 1,
            borderTopRightRadius: 5,
            borderTopLeftRadius: 5
        },
        cardView: {
            marginHorizontal: 10,
            marginVertical: 4,
            flexDirection: "row",
            justifyContent: 'space-between'
        },
        title: {
            color: colors.darkText,
            fontSize: 19,
        },
        subTitle: {
            color: colors.mediumText,
            fontSize: 13,
        },
        reverse: { 
            flexDirection: "column-reverse"
        },

    })
    
    return (
        <>
            <View style={styles.searchContainer}>
                <TextInput
                    placeholder={"Search..."}
                    style={styles.searchTextInput}
                    value={textSearch}
                    textBreakStrategy={"simple"}
                    onChangeText={setTextSearch}
                    placeholderTextColor={colors.mediumText}
                />
                <TouchableOpacity onPress={searchClick} style={styles.searchTouch}>
                    <Image
                        source={require("../../assets/images/next_icon.png")}
                        style={styles.iconNext}
                    />
                </TouchableOpacity>
            </View>

            <FlatList
                data={enterprise}
                contentContainerStyle={{
                    alignItems: 'center',
                    backgroundColor: colors.background,
                }}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                refreshing={loading}
                ListHeaderComponent={() => <SearchList enterpriseLength={enterprise?.length || 0}  onPress={() => handlerCloseSearch()} searchText={textSearched}/>}
                onRefresh={() => getDataCompanies()}
                keyExtractor={(value) => `${value.id}_${value.description}`}
                refreshControl={
                    <RefreshControl
                    refreshing={loading}
                    style={styles.refreshControl}
                    onRefresh={() => getDataCompanies()}
                    />
                }
                renderItem={(value) => {
                    const { item } = value
                    const { 
                        city,
                        country,
                        enterprise_name,
                        enterprise_type: { enterprise_type_name },
                        photo,
                    }: Enterprise = item
                    const uri = `${BASE_URL}${photo}`

                    return (
                        <TouchableOpacity
                            style={styles.card}
                            onPress={() => handlerClickCard(item)}
                        >
                            <Image style={styles.image} source={{ uri }} />
                            <View style={styles.cardView}>
                                <View>
                                    <Text style={styles.title}>
                                    {enterprise_name}
                                    </Text>
                                    <Text style={styles.subTitle}>
                                    {enterprise_type_name}
                                    </Text>
                                </View>
                                <View style={styles.reverse}>
                                    <Text style={styles.subTitle}>
                                    {city} - {country}
                                    </Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )
                }}
            />
            
            <Warning onPress={() => handlerCloseSearch()} enterpriseLength={loading || enterprise?.length || 0 } />
            <PopUp
                enterprise={selectedEnterprise}
                setClose={() => setSelectedEnterprise(null)}
            />
            
        </>
    )
}

export default Companies