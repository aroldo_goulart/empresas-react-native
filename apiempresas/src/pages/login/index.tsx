import React, { useState } from 'react'
import { View, Text, Image, Dimensions, StyleSheet, Alert } from 'react-native'
import { useNavigation } from '@react-navigation/core'
import { TextInput, TouchableOpacity, } from 'react-native-gesture-handler'
import { CommonActions, useTheme } from '@react-navigation/native'
import { Login } from '../../services/API'

interface LoginScreenProps {

}

function LoginScreen(props: LoginScreenProps) {
    const { width } = Dimensions.get("screen")
    const { colors } = useTheme();

    const navigation = useNavigation()

    const [emailError, setEmailError] = useState<boolean>(false)
    const [passwordError, setPasswordError] = useState<boolean>(false)
    const [email, setEmail] = useState<string>("")
    const [password, setPassword] = useState<string>("")
    const [loginError, setLoginError] = useState<string>('')

    const styles = StyleSheet.create({
        container: { 
            backgroundColor: colors.background,
            alignContent: "center",
            alignItems: "center",
            flex: 1,
            justifyContent: "center"
        },
        card: { 
            margin: 10, 
            backgroundColor: colors.card, 
            padding: 14,
            alignItems: "center",
            borderRadius: 5,
            minWidth: width*0.89,
            maxWidth: width*0.89
        },
        image: {
            width: 230,
            height: 100,
        },
        formView: {
            paddingHorizontal: 45,
            paddingVertical: 20,
        },
        text: {
            fontSize: 19,
            color: colors.darkText
        },
        textInput: {
            color: colors.darkText,
            borderBottomWidth: 0.4,
            borderColor: colors.darkText,
            minWidth: width*0.6,
            maxWidth: width*0.89
        },
        buttonView: {
            backgroundColor: colors.primary,
            alignContent: "center",
            alignItems: "center",
            paddingVertical: 15,
            borderRadius: 5,
            marginTop: 30
        },
        textButton: {
            color: colors.lightText,
            fontSize: 15
        }
    })

    const handleLogin = async () => {
        if(!email || !password ) {
            if(!email) {
                setEmailError(true)
            }
            else {
                setEmailError(false)
            }
            if(!password) {
                setPasswordError(true)
            }
            else {
                setPasswordError(false)
            }
        }
        else {
            setPasswordError(false)
            setEmailError(false)
         
            const { success, errors } = await Login(email, password)
            if(success) {
                navigation.dispatch(
                    CommonActions.reset({
                        index: 1,
                        routes: [
                        { name: 'Companies' },
                        ],
                    })
                );
            }
            else {
                setLoginError(errors[0])
            }
        }
    }


    return (
        <View style={styles.container}>
            <View style={styles.card}>
                <Image 
                    source={{ uri: 'https://bytebucket.org/aroldo_goulart/empresas-react-native/raw/be4c75f531c284e2bf0210e5c6d0da19f5384b63/logo_ioasys.png' }}
                    style={styles.image}
                />
                <View style={styles.formView}>
                    <Text style={styles.text}>
                    Login
                    </Text>
                    <TextInput
                        style={styles.textInput}
                        placeholder={"Type your email"}
                        placeholderTextColor={colors.mediumText}
                        value={email}
                        autoCompleteType={'email'}
                        textContentType={"emailAddress"}
                        onChangeText={(text) => setEmail(text)}
                    />  

                    <Text style={[styles.text, { marginTop: 20 }]}>
                    Senha
                    </Text>
                    <TextInput
                        style={styles.textInput}
                        placeholder={"Type your password"}
                        placeholderTextColor={colors.mediumText}
                        value={password}
                        autoCompleteType={"password"}
                        secureTextEntry
                        textContentType={"password"}
                        onChangeText={(text) => setPassword(text)}
                    />
                    {
                        passwordError || emailError ? (
                            <Text style={{ 
                                color: colors.errorText,
                                marginLeft: 6
                                }}>
                            {passwordError ? 'Password': null} {emailError ? `${passwordError ? 'and Email ' : 'Email '}` : null } 
                            {`${passwordError && "invalid" }`}
                            </Text>
                        )
                        : loginError ? (
                            <Text style={{ 
                                color: colors.errorText,
                                marginLeft: 6
                                }}>
                            {loginError}
                            </Text>
                        ): null
                    }

                    <TouchableOpacity onPress={handleLogin} style={styles.buttonView}>
                        <Text style={styles.textButton} > 
                        Login
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}
export default LoginScreen