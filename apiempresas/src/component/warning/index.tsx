import React from 'react'
import { useTheme } from '@react-navigation/native';
import { View, Text, StyleSheet, TouchableOpacity, Alert } from 'react-native'

interface Warning {
    enterpriseLength: number,
    onPress: () => void
}

function Warning(props:Warning){
    const { colors } = useTheme()
    const { enterpriseLength, onPress } = props;

    const styles = StyleSheet.create({
        card: {
            backgroundColor: colors.card,
            margin: 20, 
            justifyContent: "center",
            alignContent: "center",
            paddingVertical: 40,
            borderRadius: 5
        },
        text: {
            alignItems: 'center',
            textAlign: 'center',
            color: colors.darkText,
            fontSize: 15,
        },
        buttonView: {
            backgroundColor: colors.primary,
            alignContent: "center",
            alignItems: "center",
            paddingVertical: 15,
            borderRadius: 5,
            marginTop: 30,
            marginHorizontal: 20,
        },
        textButton: {
            color: colors.lightText,
            fontSize: 15
        }
    })

    if(enterpriseLength) {
        return null
    }
   
    return (
        <View style={styles.card}>
            <Text style={styles.text}>
            No data found for this search
            </Text>
            <TouchableOpacity onPress={() => onPress()} style={styles.buttonView}>
                <Text style={styles.textButton} > 
                Okay
                </Text>
            </TouchableOpacity>
        </View>
    )
}

export default Warning
