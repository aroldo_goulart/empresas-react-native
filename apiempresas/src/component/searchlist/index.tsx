import React from 'react'
import { useTheme } from '@react-navigation/native';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'

interface SearchList {
    searchText: string,
    onPress: () => void,
    enterpriseLength: number
}

function SearchList(props:SearchList){
    const { colors } = useTheme()
    const { searchText, onPress, enterpriseLength }= props

    const styles = StyleSheet.create({
        container: {
            backgroundColor: colors.primary,
            padding: 10,
            marginTop: 10,
            borderRadius: 5,
            flexDirection: "row",
            alignItems: "center"
        },
        textSearch: {
            color: colors.lightText,
            textAlign: "center",
            marginHorizontal: 2,
            marginRight: 6,
            fontSize: 16
        },
        imageView: {
            backgroundColor: colors.card,
            padding: 5,
            marginHorizontal: 2,
            borderRadius: 20
        },
        image: {
            width: 12,
            height: 12
        }
    })
    
    if(!searchText || !enterpriseLength) {
        return null
    }
    return (
        <TouchableOpacity 
            onPress={() => onPress()}
            style={styles.container}>
            <Text style={styles.textSearch}>
            { searchText }
            </Text>
            <View style={styles.imageView}>
                <Image 
                    style={styles.image}
                    source={require("../../assets/images/close_icon.png")}
                />
            </View>
        </TouchableOpacity>
    )
}

export default SearchList
