import { useTheme } from '@react-navigation/native';
import React from 'react'
import { StyleSheet, Text } from 'react-native'

interface ContactProps {
    link?: string | null
}

export default function Contact(props: ContactProps) {
    const { colors } = useTheme();
    const { link } = props
    const styles = StyleSheet.create({
        textContact: {
            color: colors.primary,
            marginTop: 2
        }
    })
    if(!link) {
        return null
    }
    
    const openLink = (value:string) => {
        return null
    }
    return (
        <Text onPress={() => openLink(link)} style={styles.textContact}>
        {link}
        </Text>
    )
}
