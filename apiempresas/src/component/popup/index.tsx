import { useTheme } from '@react-navigation/native'
import React, { useState } from 'react'
import { View, Text, Dimensions, Pressable, Image, StyleSheet, Touchable, Animated, StyleProp, ViewStyle } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { BASE_URL } from '../../services/API'
import { Enterprise } from '../../services/types'
import Contact from '../contact'

interface PopUPProps {
    enterprise: Enterprise,
    setClose: () => void,
}
function PopUp(props: PopUPProps) {
    const { width, height } = Dimensions.get("screen")
    const { colors } = useTheme();
    const { enterprise, setClose } = props
    const styles = StyleSheet.create({
        container: {
            position: 'absolute',
            width,
            height,
            backgroundColor: "rgba(0, 0, 0, 0.7)",
            alignSelf: "center",
        },
        card: {
            position: 'absolute',
            width: width *0.9,
            height: height*0.7,
            backgroundColor: colors.card,
            alignSelf: "center",
            top: height*0.1,
            borderRadius: 10,
        },
        image: {
            height: 160,
            width: width*0.9,
            borderTopLeftRadius: 5,
            borderTopRightRadius: 5
        },
        information: {
            margin: 15
        },
        titleText: {
            fontSize: 18,
            color: colors.darkText
        },
        subTitleText: {
            fontSize: 14,
            color: colors.mediumText
        },
        row: {
            flexDirection: 'row',
            justifyContent: "space-between"
       },
       spacing: {
           marginTop: 25
       },
       buttonView: {
            backgroundColor: colors.primary,
            alignContent: "center",
            alignItems: "center",
            paddingVertical: 15,
            marginHorizontal: 15,
            marginVertical: 10,
            borderRadius: 5
        },
        textButton: {
            color: colors.lightText,
            fontSize: 15
        }
    })

    if(!enterprise) {
        return null
    }
    const { 
        city,
        country,
        description,
        email_enterprise,
        enterprise_name,
        enterprise_type: {
            enterprise_type_name
        },
        facebook,
        id,
        linkedin,
        own_enterprise,
        phone,
        photo,
        twitter,
    } = enterprise
    const [uri] = useState(`${BASE_URL}${photo}`)
    const [ fade ] = useState(new Animated.Value(0))  

    const opacity = fade.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 2]
    })

    function animate() {
        Animated.timing(fade, {
            toValue: 1,
            duration: 500,
            useNativeDriver: true,
        }).start()
    }

    return (
        <Animated.View 
            key={`${id}_${city}_${country}`} 
            style={[styles.container, { opacity }]}
            onLayout={() => animate()}
        >
            <View style={styles.card}>
            <Image 
                source={{ uri }}
                style={styles.image}
            />
            <ScrollView  
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            style={styles.information}>
                <View style={styles.row}> 
                    <View>
                        <Text style={styles.titleText}>
                        {enterprise_name}
                        </Text>
                        <Text style={styles.subtitleText}>
                        {enterprise_type_name}
                        </Text>
                    </View>
                    <View>
                        <Text style={[styles.subTitleText, { textAlign: 'right'}]}>
                        {country}
                        </Text>
                        <Text style={[styles.subTitleText, { textAlign: 'right'}]}>
                        {city}
                        </Text>
                    </View>
                </View>

                <View style={styles.spacing}>
                    <Text style={styles.titleText}>
                    Description
                    </Text>
                    <Text style={styles.subTitleText}>
                    {description}
                    </Text>
                </View>

                <View style={styles.spacing}>
                    {
                        facebook || linkedin || twitter || phone ? (
                            <Text style={styles.titleText}>
                                Contacts
                            </Text>
                        )
                        : null
                    }
                    <Contact link={facebook}/>
                    <Contact link={linkedin}/>
                    <Contact link={twitter}/>
                    <Contact link={phone}/>
                </View>
            </ScrollView>
            <TouchableOpacity onPress={() => setClose()} style={styles.buttonView}>
                <Text style={styles.textButton} > 
                Close
                </Text>
            </TouchableOpacity>
            
        </View>
        
    </Animated.View>
    )
}

export default PopUp